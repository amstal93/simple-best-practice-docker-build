ARG BUILD_TYPE=base
# Options: base, extended, docker, docker-extended
# base: ~985Mb
# docker: +~21Mb
# extended: +~200Mb

FROM gcr.io/google.com/cloudsdktool/cloud-sdk:latest AS build-base

FROM build-base AS build-extended
ONBUILD RUN pip install numpy scipy lxml

FROM build-base AS build-docker
ONBUILD RUN apt-get install -qq file docker.io

FROM build-docker AS build-docker-extended
ONBUILD RUN pip install numpy scipy lxml

FROM build-${BUILD_TYPE}

ENV BUILD_TYPE=${BUILD_TYPE}

ARG CI_COMMIT_SHA
ARG CI_PROJECT_URL
ARG CI_COMMIT_REF_NAME
ARG CI_PIPELINE_URL
ARG CI_JOB_URL
ARG CI_MERGE_REQUEST_ID
ARG CI_REGISTRY_IMAGE
ARG GITLAB_USER_NAME
ARG GITLAB_USER_EMAIL
ARG BUILD_DATE
ARG BUILD_TITLE

# PYTHON_VERSION should be specified by passing `--build-arg PYTHON_VERSION=<new version>` to docker build
# EWB - This is a reasonable default as of 2020-07-01
# Verify ARGS @ https://microbadger.com/
ARG PYTHON_VERSION=3.8.3
ARG MAINTAINER="GDT PaaS <paas@groupm.tech>"

ENV PYTHON_VERSION=${PYTHON_VERSION}

# http://bugs.python.org/issue19846
ENV LANG C.UTF-8

LABEL org.opencontainers.image.vendor="$GITLAB_USER_NAME"
LABEL org.opencontainers.image.authors="$GITLAB_USER_NAME"
LABEL org.opencontainers.image.revision="$CI_COMMIT_SHA"
LABEL org.opencontainers.image.source="$CI_PROJECT_URL"
LABEL org.opencontainers.image.documentation="$CI_PROJECT_URL"
LABEL org.opencontainers.image.licenses="$CI_PROJECT_URL"
LABEL org.opencontainers.image.url="$CI_PROJECT_URL"
LABEL org.opencontainers.image.created="$BUILD_DATE"
LABEL org.opencontainers.image.title="$BUILD_TITLE"
LABEL org.opencontainers.image.description="$BUILD_TITLE"
LABEL org.opencontainers.image.ref.name="$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"

LABEL com.gitlab.ci.user="$GITLAB_USER_NAME"
LABEL com.gitlab.ci.email="$GITLAB_USER_EMAIL"
LABEL com.gitlab.ci.tagorbranch="$CI_COMMIT_REF_NAME"
LABEL com.gitlab.ci.pipelineurl="$CI_PIPELINE_URL"
LABEL com.gitlab.ci.commiturl="$CI_PROJECT_URL/commit/$CI_COMMIT_SHA"
LABEL com.gitlab.ci.cijoburl="$CI_JOB_URL"
LABEL com.gitlab.ci.mrurl="$CI_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_ID"
LABEL tech.groupm.vcs-ref="$CI_PROJECT_URL"
LABEL tech.groupm.build-date="${BUILD_DATE}"
LABEL tech.groupm.build-type="${BUILD_TYPE}"
LABEL tech.groupm.maintainer="${MAINTAINER}"

ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

RUN apt-get update
RUN apt-get -qq upgrade

# Eliminate bogus apt warning
RUN apt-get install -qq -y --no-install-recommends apt-utils

# Additional Python dependencies
RUN apt-get install -qq --no-install-recommends \
    make \
    build-essential \
    libssl-dev \
    zlib1g-dev \
    libbz2-dev \
    libreadline-dev \
    libsqlite3-dev \
    wget \
    curl \
    llvm \
    libncurses5-dev \
    uuid-dev \
    xz-utils \
    tk-dev \
    libxml2-dev \
    libxmlsec1-dev \
    libffi-dev \
    liblzma-dev \
    libbluetooth-dev 

# Alternate packages for debian-slim
# dpkg-dev \
# gcc \
# libbluetooth-dev \
# libc6-dev \
# libexpat1-dev \
# libgdbm-dev \
# libncursesw5-dev \

# Pyenv dependencies
RUN apt-get install -qq ca-certificates git

# Install pyenv
RUN curl https://pyenv.run | bash
ENV PATH="/root/.pyenv/shims:/root/.pyenv/bin:${PATH}"
ENV PYENV_SHELL=bash

# Install python
RUN set +e; pyenv install $PYTHON_VERSION; set -e
RUN pyenv global $PYTHON_VERSION
# Shouldn't be needed, but harmless and avoids reported weirdness
RUN pyenv rehash

# Install pip and requirements
RUN curl https://bootstrap.pypa.io/get-pip.py | python

RUN mkdir /app
COPY . /app
WORKDIR /app
RUN rm -rf .python-version
ENTRYPOINT ["bash"]